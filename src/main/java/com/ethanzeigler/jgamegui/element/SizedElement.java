package com.ethanzeigler.jgamegui.element;

/**
 * Created by ethanzeigler on 4/5/16.
 */
public interface SizedElement {
    /**
     * Gets the width of the element
     * @return the element width
     */
    double getWidth();

    /**
     * Gets the height of the element
     * @ the element height
     */
    double getHeight();
}
